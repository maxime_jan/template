var GrovePi = require('node-grovepi').GrovePi;

var Commands = GrovePi.commands;

var Board = GrovePi.board;
var board;

var dhtSensor;
var barometerSensor;

var DHTDigitalSensor = GrovePi.sensors.DHTDigital;
var BarometerI2CSensor = GrovePi.sensors.BarometerI2C;

var start = function() {

    console.log("Démarrage");

    board = new Board({

            debug: true,

            onError: function(err) {
                console.log(err);
            },

            onInit: function(res) {

                dhtSensor = new DHTDigitalSensor(4, DHTDigitalSensor.VERSION.DHT22, DHTDigitalSensor.CELSIUS);

                barometerSensor = new BarometerI2CSensor(BarometerI2CSensor.VERSION.BMP085);

                // Temps en millisecondes
                dhtSensor.watch(100);
                barometerSensor.watch(100);



            } else {
                console.log('TEST CANNOT START');
            }
        }
    })

board.init()

}

function onExit(err) {
    console.log('ending');
    board.close();
    process.removeAllListeners();
    process.exit();
    if (typeof err != 'undefined')
        console.log(err);
}

//Détecte l'arrêt du programme
process.on('SIGINT', onExit);