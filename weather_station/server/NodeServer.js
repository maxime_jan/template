var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var fs = require('fs');

var port = 8080;

app.get('/', function (req, res) {
  res.sendFile("/home/pi/tm-maxime-jan/source/client/html/index.html");
});

app.use(express.static("/home/pi/tm-maxime-jan/source/client/static"));

server.listen(port, function() {
    console.log("Un serveur a été démarré sur le port" + port);

});

var socketIOFile = require("./socketio")(io);

