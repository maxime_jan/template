function FindLowest(dataList, size) {
    var min = [100000, 100000];
    for (var j = 0; j < size; j++) {
        for (var i = 0; i < dataList.length; i++) {
            if (dataList[i][j + 1] < min[j]) {
                min[j] = dataList[i][j + 1];
            }
        }
    }
    return min;
}

function FindHighest(dataList, size) {
    var max = [-100000, -100000];
    console.log(size)
    for (var j = 0; j < size; j++) {
        for (var i = 0; i < dataList.length; i++) {
            if (dataList[i][j + 1] > max[j]) {
                max[j] = dataList[i][j + 1];
            }
        }
    }
    return max;
}