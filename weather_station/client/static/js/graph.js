//Fonction qui dessine le graphe
function drawChart(dataList) {
    
}

socket.on("returnData", function(data) {

    google.setOnLoadCallback(drawChart(data));


});

var options = {
        //Permet d'avoir des courbes et non des traits tout droit
        curveType: 'function',
        hAxis: {
            gridlines: {
                count: -1,
                units: {
                    days: {
                        format: ['yyyy/MM/dd']
                    },
                    hours: {
                        format: ["MM/dd HH:00",
                            "HH"
                        ]
                    },

                    minutes: {
                        format: [
                            "HH:mm"
                        ]
                    },
                }
            },
            minorGridlines: {
                units: {
                    hours: {
                        format: ['hh:mm:ss']
                    },
                    minutes: {
                        format: ['HH:mm']
                    }
                }
            }
        },
        series: {
            0: {
                targetAxisIndex: 0
            }, //Permet d'indexer chaque axe Y
            1: {
                targetAxisIndex: 1
            }
        }
    };